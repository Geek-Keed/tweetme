# Roadmap of the project

**Tweets:**
  1. -> Creating
     1. -> Text
     2. -> Image
  2. -> Delete
  3. -> Retweeting
     
**Users:**
  1. -> Register
  2. -> Login
  3. -> Logout
  4. -> Profile
     1. -> Image?
     2. -> Text?
     3. -> Follow Button
  5. -> Feed
     1. -> User's feed only?
     2. -> User+ who they follow         

**Following / Followers**
**Long term todos:**
  1. Notifications
  2. -> DM
  3. -> Explore -> Finding hashtags
